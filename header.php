<?php
/**
 * The Header for our child theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Responsive Twenty_Ten
 * @since Responsive Twenty Ten 0.1
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
<meta name="google-site-verification" content="ocO6qmxvQUygb6_AaEkvUyoYB3K6qPDfGZtGtToZ6g0" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet"> 
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/css_browser_selector.min.js"></script>

<?php
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
?>
<?php wp_head(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-2105788-5', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body <?php body_class(); ?>>
<header class="fullwidth" role="banner">
	<div class="container">
    
<a id="logo" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php if(is_front_page()){?><h1>Okanagan Railings | Glass Railings and Glass Railing Installations</h1><?php } ?><img class="logo" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" /></a>

<?php dynamic_sidebar( 'header-widget-area' ); ?>
            
	</div>
</header>

<nav role="navigation">
<div class="container">
	<div class="skip-link screen-reader-text"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentyten' ); ?>"><?php _e( 'Skip to content', 'twentyten' ); ?></a></div>
	<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
</div>			
</nav>

<?php if(is_front_page()){ ?>
<div id="slider">
	<div class="lg-slick-slider-feature" style="width:100%; max-width: 100%;display:block;">


		<?php $slide_shortcode = get_field('slide_shortcode', 'option'); ?>
		<?php $slide_text = get_field('slide_text', 'option'); ?>
		
		<?php while ( have_rows('feature_slider', 'option') ) : the_row(); ?>
	
				<?php $feature_slide = get_sub_field('feature_slide', 'option'); ?>
				
				<?php $slide_mobile_link = get_sub_field('slide_mobile_link', 'option'); ?>

			<div class="feature-slide">
				<img src="<?php echo $feature_slide['url']; ?>" alt="<?php echo $feature_slide['alt']; ?>" >
				<div class="slide-overlay caption">
					<a href="<?php echo $slide_mobile_link['url'] ?>" class="mobile-show"><?php echo $slide_mobile_link['title'] ?></a>
				</div>
			</div>
		<?php endwhile ?>
	</div>
	<div class="slide-form">
				<h3 class="slide-overlay caption"><?php echo $slide_text ?></h3>
				<?php echo do_shortcode("$slide_shortcode") ?>
	</div>
</div>
<?php } ?>
