<?php
function child_twentyten_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Header Area', 'twentyten' ),
		'id' => 'header-widget-area',
		'description' => __( 'Add widgets here to appear in your header.', 'twentyten' ),
		'before_widget' => '<section id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );
	register_sidebar( array(
		'name' => __( 'Sidebar Area', 'twentyten' ),
		'id' => 'primary-widget-area',
		'description' => __( 'Add widgets here to appear in your sidebar.', 'twentyten' ),
		'before_widget' => '<section id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Area', 'twentyten' ),
		'id' => 'footer-widget-area',
		'description' => __( 'These widgets will show up on your footer', 'twentyten' ),
		'before_widget' => '<section id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );
}
//AFTER THEME IS SET UP (FUNCTIONS.PHP IS LOADED FROM PARENT THEME) - REMOVE THE SIDEBAR WIDGETS
add_action('after_setup_theme','remove_parent_widgets');
function remove_parent_widgets() {
    remove_action( 'widgets_init', 'twentyten_widgets_init' );
}
//RE-ADD THE SIDEBAR WIDGETS WITH THE NEW CODE ADDED, DUPLICATING THE FUNCTION
add_action( 'after_setup_theme', 'child_twentyten_widgets_init' );

//ADD REALLY SIMPLE CAPTCHA BACK TO CONTACT FORM 7
add_filter( 'wpcf7_use_really_simple_captcha', '__return_true' );

//CHANGE DEFAULT THEME NAME
add_filter('default_page_template_title', function() {
    return __('One column, right sidebar', 'your_text_domain');
});

//REMOVES HENTRY FROM PAGES TO ELIMINATE ERRORS IN GOOGLE WEBMASTER TOOLS
function themeslug_remove_hentry( $classes ) {
    if ( is_page() ) {
        $classes = array_diff( $classes, array( 'hentry' ) );
    }
    return $classes;
}
add_filter( 'post_class','themeslug_remove_hentry' );

//ADDS NEW IMAGE CROP SIZE TO WORDPRESS
add_image_size( 'projects-thumb-crop', 450, 300, true ); // Hard Crop Mode


function lg_enqueue_styles_scripts() {
    wp_enqueue_style( 'slickcss', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', null );

    wp_enqueue_style( 'slickcss-theme', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css

', null );

	wp_register_script( 'slickjs', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js',null  );
	wp_enqueue_script( 'slickjs' );

	wp_register_script( 'lg-script', get_stylesheet_directory_uri() . "/js/longevity.js", array('jquery'), filemtime(get_stylesheet_directory() . '/js/longevity.js') );
	wp_enqueue_script( 'lg-script' );

}
add_action( 'wp_enqueue_scripts', 'lg_enqueue_styles_scripts' );


add_shortcode('lg-slickgallery', 'lg_slickgallery_func' );



function lg_slickgallery_func() {

	ob_start();

	?>
	<style>
		.content{
			display: flex;
		}
		#content{
			display: block !important;
			width: calc(100% - 325px)  !important;
		}
		aside{
			display: block;
		}
	</style>
	<div class="lg-slickgallery" style="width:100%; max-width: 100%;">
	<div class="lg-slick-slider-main" style="width:100%; max-width: 100%;display:block;">

		<?php while ( have_rows('gallery_slider', 'option') ) : the_row(); ?>

			<?php $gallery_slide = get_sub_field('gallery_slide', 'option'); ?>

			<div class="main-slide">
				<img style="width:100%; max-width: 100%" src="<?php echo $gallery_slide['url']; ?>" alt="<?php echo $gallery_slide['alt']; ?>" >
			</div>

		<?php endwhile ?>

	</div>

	<div class="nav-slider">

		<?php while ( have_rows('gallery_slider', 'option') ) : the_row(); ?>

			<?php $gallery_slide = get_sub_field('gallery_slide', 'option'); ?>

			<div class="nav-slide">
				<img src="<?php echo $gallery_slide['url']; ?>" alt="<?php echo $gallery_slide['alt']; ?>" >
			</div>

		<?php endwhile ?>
	
	</div>
	</div>
	
	<?php
	return ob_get_clean();
}

function create_menu() {
    add_menu_page(
        __( 'Okanagan Railings', 'okanaganrailings' ),     ### Need change
        'Okanagan Railings',        ### Need change
        'manage_options',
        'okanaganrailings',   ### Need change
        '',
        'dashicons-admin-site',
        2
    );
}

add_action( 'admin_menu', 'create_menu' );

?>
<?php 

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Global Theme Settings',
		'menu_title'	=> 'Global Theme Settings',
		'parent_slug'	=> 'okanaganrailings',
	));
	
	
}

?>
