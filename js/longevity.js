 jQuery(window).ready(function(){

  jQuery('.lg-slick-slider-main').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: false,
    asNavFor: '.nav-slider'
  
  });

  jQuery('.lg-slick-slider-feature').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: false
  
  });

  jQuery('.nav-slider').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  dots: false,
  centerMode: true,
  focusOnSelect: true,
  asNavFor: '.lg-slick-slider-main',
  responsive: [
    {
      breakpoint: 1440,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 425,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  
  });

});
