<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<section class="content">
	<main id="content" role="main">

			<article id="post-0" class="post error404 not-found">
				<h1 class="entry-title">404 ERROR: <?php _e( 'Not Found', 'twentyten' ); ?></h1>
				<div class="entry-content">
					<p><?php _e( 'Apologies, but the page you requested could not be found.', 'twentyten' ); ?></p>
					<p><a href="/">&larr; Back to Homepage</a></p>
				</div><!-- .entry-content -->
			</article>

	</main>
</section>
    
	<script type="text/javascript">
		// focus on search field after it has loaded
		document.getElementById('s') && document.getElementById('s').focus();
	</script>

<?php get_footer(); ?>