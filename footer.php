<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<div class="btmlinks">
  <div class="container">
    <?php dynamic_sidebar( 'footer-widget-area' ); ?>
  </div>
</div>

<footer class="fullwidth" role="contentinfo">
	<div class="container">
  
<div id="copyright">  
<?php
	get_sidebar( 'footer' );
?>
				Copyright &copy; <a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a> All rights reserved. | <a href="/privacy-policy">Privacy Policy</a>
</div><div id="longevity">
<!-- <a target="_blank" href="http://www.longevitygraphics.com">Vancouver Website Design</a> by <a target="_blank" href="http://www.longevitygraphics.com">Longevity Graphics</a> -->
</div>
                
	</div>
</footer>

<?php wp_footer(); ?>
<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 942934397;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/942934397/?guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
</html>