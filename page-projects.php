<?php
/**
 * Template Name: Projects
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<section class="content">
		<main id="content" class="one-column" role="main">
        
			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
			?>
            
<div id="projects" class="grid">

<?php 
    query_posts(array( 
	  'posts_per_page' => 100,
	  'post_type' => 'projects',
	  'order' => DESC
    ) );  
while (have_posts()) : the_post();
?>

<div class="project col">
<div class="view third-effect">
<?php the_post_thumbnail('projects-thumb-crop'); ?>
<a href="<?php the_permalink() ?>"><div class="mask"></div></a>
</div>

<h2 class="proj-title"><?php the_title(); ?></h2>
</div>

<?php endwhile; ?>

</div><!-- .grid -->
<div style="clear:both;"></div>

		</main>

</section>

<?php get_footer(); ?>